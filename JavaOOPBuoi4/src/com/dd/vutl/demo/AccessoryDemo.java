package com.dd.vutl.demo;

import java.util.ArrayList;

import com.dd.vutl.dao.Database;
import com.dd.vutl.entity.Accessory;
import com.dd.vutl.entity.BaseRow;

public class AccessoryDemo {
	static Database database = new Database();

	public static void main(String[] args) {
		createAccessoryTest();
		selectAccessoryTest();
		printAccessory();
	}

	public static void createAccessoryTest() {

		Accessory accessory = new Accessory(1, "Accessory1");
		Accessory accessory2 = new Accessory(2, "Accessory2");
		Accessory accessory3 = new Accessory(3, "Accessory3");
		Accessory accessory4 = new Accessory(4, "Accessory4");
		Accessory accessory5 = new Accessory(5, "Accessory5");
		database.insertTable("accessory", accessory);
		database.insertTable("accessory", accessory2);
		database.insertTable("accessory", accessory3);
		database.insertTable("accessory", accessory4);
		database.insertTable("accessory", accessory5);
	}

	public static void selectAccessoryTest() {
		ArrayList<BaseRow> accessory = database.selectTable("accessory");
		System.out.println(accessory);
	}

	public static void printAccessory() {
		System.out.println(new Accessory(1, "Accessory1"));

	}
}
