package com.dd.vutl.demo;

import java.util.ArrayList;

import com.dd.vutl.dao.CategoryDAO;
import com.dd.vutl.entity.BaseRow;
import com.dd.vutl.entity.Category;

public class CategoryDaoDemo {
	static CategoryDAO categoryDao = new CategoryDAO();

	public static void main(String[] args) {
		insertCategoryTest();
		updateCategoryTest();
		deleteCategoryTest();
		findAllTest();
		System.out.println("Find by ID:");
		findByIDTest();
		System.out.println("Find by Name:");
		findByNameTest();
	}

	private static Integer insertCategoryTest() {
		Category category = new Category(1, "Category1");
		Category category2 = new Category(2, "Category2");
		Category category3 = new Category(3, "Category3");
		Category category4 = new Category(4, "Category4");
		Category category5 = new Category(5, "Category5");
		int i;
		if (categoryDao.insert(category) == 1 && categoryDao.insert(category2) == 1
				&& categoryDao.insert(category3) == 1 && categoryDao.insert(category4) == 1 && categoryDao.insert(category5) == 1) {
			i = 1;
			System.out.println("Insert Success");
		} else {
			i = 0;
			System.out.println("Insert Fail");
		}
		return i;
	}

	private static Integer updateCategoryTest() {
		ArrayList<BaseRow> listcategory = categoryDao.findAll();
		Category oldcategory = (Category) listcategory.get(1);
		Category newcategory = new Category(8,"category 8");
		int i = categoryDao.update(oldcategory, newcategory);
		if(i==1) {
			System.out.println("Update Success");
		}else {
			i = 0;
			System.out.println("Update Fail");
		}
		return i;

	}

	private static boolean deleteCategoryTest() {
		ArrayList<BaseRow> listcategory = categoryDao.findAll();
		Category category = (Category) listcategory.get(3);
		boolean check = categoryDao.delete(category);
		System.out.println("Delete : "+category+" "+check);
		return check;

	}
	
	private static ArrayList<BaseRow> findAllTest() {
		ArrayList<BaseRow> listCategory= categoryDao.findAll();
		System.out.println(listCategory);
		return listCategory;
	}
	
	private static void findByIDTest() {
		Category category = categoryDao.findById(3);
		System.out.println(category);
	}
	
	private static void findByNameTest() {
		Category category = categoryDao.findByName("Category5");
		System.out.println(category);
	}
}
