package com.dd.vutl.dao;

import java.util.ArrayList;

import com.dd.vutl.entity.BaseRow;

public abstract class BaseDao implements IDao {
	private Database database = Database.getInstance();
	protected String tableName;

	protected BaseDao() {
		this.tableName="product";
		this.tableName="category";
		this.tableName="accessory";
		
	}
	
	protected Integer insert(BaseRow row) {
		int i = database.insertTable(tableName, row);
		return i;
	}
	
	protected Integer update(BaseRow oldRow, BaseRow newRow) {
		int i = database.updateTable(tableName, oldRow, newRow);
		return i;
	}

	protected boolean delete(BaseRow row) {
		boolean check = database.deleteTable(tableName, row);
		return check;
	}

	@Override
	public ArrayList<BaseRow> findAll() {
		ArrayList<BaseRow> listAccessory = database.selectTable(tableName);
		return listAccessory;
	}

	@Override
	public BaseRow findById(int id) {
		ArrayList<BaseRow> listBaserows = database.selectTable(tableName);
		BaseRow baserows = listBaserows.stream().filter(baserow -> id == baserow.getId()).findAny()
				.orElse(null);
		return baserows;
	}

	@Override
	public BaseRow findByName(String name) {
		ArrayList<BaseRow> listBaserows = database.selectTable(tableName);
		BaseRow baserows = listBaserows.stream().filter(baserow -> name.equals(baserow.getName())).findAny()
				.orElse(null);
		return baserows;
	}

}
