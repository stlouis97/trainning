package com.dd.vutl.dao;

import java.util.ArrayList;

import com.dd.vutl.entity.BaseRow;
import com.dd.vutl.entity.Product;

public class ProductDAO extends BaseDao {

	public ProductDAO() {
		super();
		this.tableName = "product";
	}

	public Integer insert(Product row) {
		return super.insert(row);
	}

	public Integer update(Product oldRow, Product newRow) {
		return super.update(oldRow, newRow);
	}

	public boolean delete(Product row) {
		return super.delete(row);
	}

	public ArrayList<BaseRow> findAll() {
		return super.findAll();
	}

	public Product findById(int id) {
		return (Product) super.findById(id);
	}

	public Product findByName(String name) {
		return (Product) super.findByName(name);
	}


//	@Override
//	public Integer insert(BaseRow row) {
//		// TODO Auto-generated method stub
//		int i;
//		i = database.insertTable("product", row);
//		return i;
//	}
//
//	@Override
//	public Integer update(BaseRow oldRow, BaseRow newRow) {
//		// TODO Auto-generated method stub
//		int i;
//		i = database.updateTable("product", oldRow, newRow);
//		return i;
//	}
//
//	@Override
//	public boolean delete(BaseRow row) {
//		// TODO Auto-generated method stub
//		boolean check;
//		check = database.deleteTable("product", row);
//		return check;
//	}
//
//	@Override
//	public ArrayList<BaseRow> findAll(){
//		ArrayList<BaseRow> listProduct = database.selectTable("product");
//		return listProduct ;
//		
//	}
//
//	@Override
//	public BaseRow findById(int id) {
//		// TODO Auto-generated method stub
//		ArrayList<BaseRow> listProducts = database.selectTable("product");
//		BaseRow products = listProducts.stream().filter(product -> id == product.getId()).findAny().orElse(null);
//		return products;
//	}
//
//	@Override
//	public BaseRow findByName(String name) {
//		// TODO Auto-generated method stub
//		ArrayList<BaseRow> listProducts = database.selectTable("product");
//		BaseRow products = listProducts.stream().filter(product -> name == product.getName()).findAny().orElse(null);
//		return products;
//	}
//		public static Integer insert(Object row) {
//			int i;
//			i = database.insertTable("product", row);
//			return i;
//		}
	//
//		public static Integer update(Object oldRow, Object newRow) {
//			int i;
//			i = database.updateTable("product", oldRow, newRow);
//			return i;
//		}
	//
//		public static boolean delete(Object row) {
//			boolean check;
//			check = database.deleteTable("product", row);
//			return check;
//		}
	//
//		public static ArrayList<Object> findAll() {
//			ArrayList<Object> listProducts = database.selectTable("product");
//			return listProducts;
//		}
	//
//		public static Object findById(int id) {
//			ArrayList<Object> listProducts = database.selectTable("product");
//			Product product1 = new Product();
//			for (int i = 0; i < listProducts.size(); i++) {
//				Product product2 = (Product) listProducts.get(i);
//				if (id == product2.getId()) {
//					product1 = product2;
//				}
//			}
//			return product1;
//		}
	//
//		public static Object findByName(String name) {
//			ArrayList<Object> listProducts = database.selectTable("product");
//			Product product1 = new Product();
//			for (int i = 0; i < listProducts.size(); i++) {
//				Product product2 = (Product) listProducts.get(i);
//				if (name.equals(product2.getName())) {
//					product1 = product2;
//				}
//			}
//			return product1;
//		}

}
