package com.dd.vutl.dao;

import java.util.ArrayList;

import com.dd.vutl.entity.Accessory;
import com.dd.vutl.entity.BaseRow;
import com.dd.vutl.entity.Category;
import com.dd.vutl.entity.Product;

public class Database {

//	public ArrayList<Object> productTable;
//	public ArrayList<Object> categoryTable;
//	public ArrayList<Object> accessoryTable;
	public ArrayList<BaseRow> productTbl;
	public ArrayList<BaseRow> categoryTbl;
	public ArrayList<BaseRow> accessoryTbl;
	private static Database instance;

	public Database() {
//		this.productTable = new ArrayList<Object>();
//		this.categoryTable = new ArrayList<Object>();
//		this.accessoryTable = new ArrayList<Object>();

		this.productTbl = new ArrayList<BaseRow>();
		this.categoryTbl = new ArrayList<BaseRow>();
		this.accessoryTbl = new ArrayList<BaseRow>();

	}

	public static Database getInstance() {
		if (instance == null) {
			instance = new Database();
		}
		return instance;
	}

	public ArrayList<BaseRow> getTable(String name) {
		ArrayList<BaseRow> baserow = new ArrayList<BaseRow>();
		if (name.equals("product")) {
			baserow = this.productTbl;
		}
		if (name.equals("category")) {
			baserow = this.categoryTbl;
		}
		if (name.equals("accessory")) {
			baserow = this.accessoryTbl;
		}
		return baserow;
	}

	public Integer insertTable(String name, BaseRow row) {
		int i = 0;
		getTable(name).add(row);
		return i;
////		}
////		if (name.equals("category")) {
////			this.categoryTbl.add((Category) row);
////			i = 1;
////		}
////		if (name.equals("accessory")) {
////			this.accessoryTbl.add((Accessory) row);
////			i = 1;
////		}
//		return i;
	}

	public ArrayList<BaseRow> selectTable(String name) {
		return getTable(name);
//		if (name.equals("category")) {
//			return this.categoryTbl;
//		}
//		if (name.equals("accessory")) {
//			return this.accessoryTbl;
//		}

	}

	public Integer updateTable(String name, BaseRow oldRow, BaseRow newRow) {
		int i = 0;
		ArrayList<BaseRow> objEnitity = selectTable(name);
		Integer index = objEnitity.indexOf(oldRow);
		getTable(name).set(index, newRow);
		i = 1;

		return i;
//		}
//		if (name.equals("category")) {
//			ArrayList<BaseRow> category = selectTable("category");
//			Integer oldCategory = category.indexOf(oldRow);
//			this.categoryTbl.set(oldCategory, newRow);
//			i = 1;
//		}
//		if (name.equals("accessory")) {
//			ArrayList<BaseRow> accessory = selectTable("accessory");
//			Integer oldAccessory = accessory.indexOf(oldRow);
//			this.accessoryTbl.set(oldAccessory, newRow);
//			i = 1;
//		}

	}

	public boolean deleteTable(String name, BaseRow row) {
		boolean check = false;
		check = getTable(name).remove(row);

//		if (name.equals("category")) {
//			check = this.categoryTbl.remove(row);
//		}
//		if (name.equals("accessory")) {
//			check = this.accessoryTbl.remove(row);
//		}
		return check;
	}

	public void truncateTable(String name) {
		getTable(name).clear();
		System.out.println("Truncate product");

//		if (name.equals("category")) {
//			this.categoryTbl.clear();
//			System.out.println("Truncate category");
//		}
//		if (name.equals("accessory")) {
//			this.accessoryTbl.clear();
//			System.out.println("Truncate accessory");
//		}
	}

	public Integer updateTable2(String name, int id, BaseRow row) {
		int check = 0;
		// Dng stream
		ArrayList<BaseRow> listEntity = selectTable(name);
		BaseRow baseRow = listEntity.stream().filter(baseRows -> id == baseRows.getId()).findAny().orElse(null);
		Integer index = listEntity.indexOf(baseRow);
		getTable(name).set(index, row);
		check = 1;

		// Khng dng stream
//			ArrayList<Object> objProduct = selectTable("product");
//			for (int i = 0; i < objProduct.size(); i++) {
//				Product product2 = (Product) objProduct.get(i);
//				if (id == product2.getId()) {
//					Integer oldIndexProduct = objProduct.indexOf(product2);
//					this.productTable.set(oldIndexProduct, row);
//					check = 1;
//				}
//			}

//		if (name.equals("category")) {
//			// Dng stream
//			ArrayList<BaseRow> listCategorys = selectTable("category");
//			BaseRow category = listCategorys.stream().filter(categorys -> id == categorys.getId()).findAny()
//					.orElse(null);
//			Integer index = listCategorys.indexOf(category);
//			this.categoryTbl.set(index, row);
//			check = 1;
		// Khng dng stream
//			ArrayList<Object> objCategory = selectTable("category");
//			for (int i = 0; i < objCategory.size(); i++) {
//				Category category2 = (Category) objCategory.get(i);
//				if (id == category2.getId()) {
//					Integer oldIndexCategory = objCategory.indexOf(category2);
//					this.categoryTable.set(oldIndexCategory, row);
//					check = 1;
//				}
////			}
//		}
//		if (name.equals("accessory")) {
//			// Dng stream
//			ArrayList<BaseRow> listAccessorys = selectTable("accessory");
//			BaseRow accessorys = listAccessorys.stream().filter(accessory -> id == accessory.getId()).findAny()
//					.orElse(null);
//			Integer index = listAccessorys.indexOf(accessorys);
//			this.accessoryTbl.set(index, row);
//			check = 1;
		// Khng dng stream
//			ArrayList<Object> objAccessory = selectTable("accessory");
//			for (int i = 0; i < objAccessory.size(); i++) {
//				Accessory accessory2 = (Accessory) objAccessory.get(i);
//				if (id == accessory2.getId()) {
//					Integer oldIndexAccessory = objAccessory.indexOf(accessory2);
//					this.accessoryTable.set(oldIndexAccessory, row);
//					check = 1;
//				}
//			}

		return check;
	}

}
