package com.dd.vutl.dao;

import java.util.ArrayList;

import com.dd.vutl.entity.BaseRow;

public interface IDao {
	public abstract ArrayList<BaseRow> findAll();
	public abstract BaseRow findById(int id);
	public abstract BaseRow findByName(String name);
}
