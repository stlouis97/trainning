package com.dd.vutl.entity;

public abstract class BaseRow {

	protected int id;
	protected String name;
	
	
	public abstract String toString();
	

	protected BaseRow(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	protected BaseRow() {
		
	}

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	

}

