package com.dd.vutl.demo;

import java.util.ArrayList;

import com.dd.vutl.dao.ProductDAO;
import com.dd.vutl.entity.BaseRow;
import com.dd.vutl.entity.Product;

public class ProductDaoDemo {
	static ProductDAO productDao = new ProductDAO();
	
	public static void main(String[] args) {
		insertProductTest();
		updateProductTest();
		deleteProductTest();
		findAllTest();
		System.out.println("Find by ID:");
		findByIDTest();
		System.out.println("Find by Name:");
		findByNameTest();
	}

	private static Integer insertProductTest() {
		Product product = new Product(1, "Product1");
		Product product1 = new Product(2, "Product2");
		Product product2 = new Product(3, "Product3");
		Product product3 = new Product(4, "Product4");
		int i;
		if (productDao.insert(product) == 1 && productDao.insert(product1) == 1 && productDao.insert(product2) == 1
				&& productDao.insert(product3) == 1) {
			i = 1;
			System.out.println("Insert Success");
		} else {
			i = 0;
			System.out.println("Insert Fail");
		}
		return i;
	}
	
	private static Integer updateProductTest() {
		ArrayList<BaseRow> listProducts = productDao.findAll();
		Product oldProduct = (Product) listProducts.get(1);
		Product newProduct = new Product(8,"Product 8");
		int i = productDao.update(oldProduct, newProduct);
		if(i==1) {
			System.out.println("Update Success");
		}else {
			i = 0;
			System.out.println("Update Fail");
		}
		return i;

	}
	
	private static boolean deleteProductTest() {
		ArrayList<BaseRow> listProducts = productDao.findAll();
		Product product = (Product) listProducts.get(0);
		boolean check = productDao.delete(product);
		System.out.println("Delete : "+product+" "+check);
		return check;

	}
	
	private static ArrayList<BaseRow> findAllTest() {
		ArrayList<BaseRow> listProduct = productDao.findAll();
		System.out.println(listProduct);
		return listProduct;
	}
	
	private static void findByIDTest() {
		Product product = productDao.findById(4);
		System.out.println(product);
	}
	
	private static void findByNameTest() {
		Product product = productDao.findByName("Product3");
		System.out.println(product);
	}

}
