package com.dd.vutl.demo;

import java.util.ArrayList;

import com.dd.vutl.dao.AccessoryDAO;
import com.dd.vutl.dao.BaseDao;
import com.dd.vutl.entity.Accessory;
import com.dd.vutl.entity.BaseRow;

public class AccessoryDaoDemo extends BaseDao {
	static AccessoryDAO accessoryDao = new AccessoryDAO();

	public static void main(String[] args) {
		insertAccessoryTest();
		updateAccessoryTest();
		deleteAccessoryTest();
		findAllTest();
		System.out.println("Find by ID:");
		findByIDTest();
		System.out.println("Find by Name:");
		findByNameTest();
	}
	
	private static Integer insertAccessoryTest() {
		int i;
		Accessory accessory = new Accessory(1, "Accessory1");
		Accessory accessory2 = new Accessory(2, "Accessory2");
		Accessory accessory3 = new Accessory(3, "Accessory3");
		Accessory accessory4 = new Accessory(4, "Accessory4");
		Accessory accessory5 = new Accessory(5, "Accessory5");
		if (accessoryDao.insert(accessory) == 1 && accessoryDao.insert(accessory2) == 1
				&& accessoryDao.insert(accessory3) == 1 && accessoryDao.insert(accessory4) == 1 && accessoryDao.insert(accessory5) == 1) {
			i = 1;
			System.out.println("Insert Success");
		} else {
			i = 0;
			System.out.println("Insert Fail");
		}
		return i;
	}

	private static Integer updateAccessoryTest() {
		int i;
		ArrayList<BaseRow> listAccessory = accessoryDao.findAll();
		Accessory oldAccessory = (Accessory) listAccessory.get(1);
		Accessory newAccessory = new Accessory(8,"Accessory 8");
		i = accessoryDao.update(oldAccessory, newAccessory);
		if(i==1) {
			System.out.println("Update Success");
		}else {
			i = 0;
			System.out.println("Update Fail");
		}
		return i;

	}

	private static boolean deleteAccessoryTest() {
		ArrayList<BaseRow> listAccessory = accessoryDao.findAll();
		Accessory accessory =(Accessory) listAccessory.get(0);
		boolean check = accessoryDao.delete(accessory);
		System.out.println("Delete : "+accessory+" "+check);
		return check;

	}
	
	private static ArrayList<BaseRow> findAllTest() {
		ArrayList<BaseRow> listAccessory= accessoryDao.findAll();
		System.out.println(listAccessory);
		return listAccessory;
	}
	
	private static void findByIDTest() {
		Accessory accessory = accessoryDao.findById(4);
		System.out.println(accessory);
	}
	
	private static void findByNameTest() {
		Accessory accessory = accessoryDao.findByName("Accessory5");
		System.out.println(accessory);
	}
}
