package com.dd.vutl.entity;

public interface IEntity {
	public abstract String toString();
	public abstract int getId();
	public abstract String getName();
}
