package com.dd.vutl.entity;

public class Accessory extends BaseRow {
	public Accessory(int id, String name) {
		super(id, name);
	}

	@Override
	public String toString() {
		return this.id+" "+this.name;
	}


}
