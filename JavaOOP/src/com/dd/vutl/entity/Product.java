package com.dd.vutl.entity;

public class Product extends BaseRow {
	
	public Product(int id, String name) {
		super(id, name);
	}

	@Override
	public String toString() {
		return this.id + " " + this.name;
	}

}
