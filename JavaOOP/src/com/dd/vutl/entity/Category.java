package com.dd.vutl.entity;

public class Category extends BaseRow {
	public Category(int id, String name) {
		super(id, name);
	}

	@Override
	public String toString() {
		return this.id+" "+this.name;
	}


}
