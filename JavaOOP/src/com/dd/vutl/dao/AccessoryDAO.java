package com.dd.vutl.dao;

import java.util.ArrayList;

import com.dd.vutl.entity.Accessory;
import com.dd.vutl.entity.BaseRow;

public class AccessoryDAO extends BaseDao {

	public AccessoryDAO() {
		super();
		this.tableName = "accessory";
	}

	public Integer insert(Accessory row) {
		return super.insert(row);
	}

	public Integer update(Accessory oldRow, Accessory newRow) {
		return super.update(oldRow, newRow);
	}

	public boolean delete(Accessory row) {
		return super.delete(row);
	}

	public ArrayList<BaseRow> findAll() {
		return super.findAll();
	}

	public Accessory findById(int id) {
		return (Accessory) super.findById(id);
	}

	public Accessory findByName(String name) {
		return (Accessory) super.findByName(name);
	}

}
