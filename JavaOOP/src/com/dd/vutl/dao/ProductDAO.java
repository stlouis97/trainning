package com.dd.vutl.dao;

import java.util.ArrayList;

import com.dd.vutl.entity.BaseRow;
import com.dd.vutl.entity.Product;

public class ProductDAO extends BaseDao {

	public ProductDAO() {
		super();
		this.tableName = "product";
	}

	public Integer insert(Product row) {
		return super.insert(row);
	}

	public Integer update(Product oldRow, Product newRow) {
		return super.update(oldRow, newRow);
	}

	public boolean delete(Product row) {
		return super.delete(row);
	}

	public ArrayList<BaseRow> findAll() {
		return super.findAll();
	}

	public Product findById(int id) {
		return (Product) super.findById(id);
	}

	public Product findByName(String name) {
		return (Product) super.findByName(name);
	}
}
