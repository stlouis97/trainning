package com.dd.vutl.dao;

import java.util.ArrayList;

import com.dd.vutl.entity.BaseRow;
import com.dd.vutl.entity.Category;

public class CategoryDAO extends BaseDao {

	public CategoryDAO() {
		super();
		this.tableName = "category";
	}

	public Integer insert(Category row) {
		return super.insert(row);
	}

	public Integer update(Category oldRow, Category newRow) {
		return super.update(oldRow, newRow);
	}

	public boolean delete(Category row) {
		return super.delete(row);
	}

	@Override
	public ArrayList<BaseRow> findAll() {
		return super.findAll();
	}

	public Category findById(int id) {
		return (Category) super.findById(id);
	}

	public Category findByName(String name) {
		return (Category) super.findByName(name);
	}
}
