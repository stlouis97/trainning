package com.dd.vutl.dao;

import java.util.ArrayList;

import com.dd.vutl.entity.Accessory;
import com.dd.vutl.entity.BaseRow;
import com.dd.vutl.entity.Category;
import com.dd.vutl.entity.Product;

public class Database {

	public ArrayList<BaseRow> productTbl;
	public ArrayList<BaseRow> categoryTbl;
	public ArrayList<BaseRow> accessoryTbl;
	private static Database instance;

	public Database() {
		this.productTbl = new ArrayList<BaseRow>();
		this.categoryTbl = new ArrayList<BaseRow>();
		this.accessoryTbl = new ArrayList<BaseRow>();

	}

	public static Database getInstance() {
		if (instance == null) {
			instance = new Database();
		}
		return instance;
	}

	public ArrayList<BaseRow> getTable(String name) {
		ArrayList<BaseRow> baserow = new ArrayList<BaseRow>();
		if (name.equals("product")) {
			baserow = this.productTbl;
		}
		if (name.equals("category")) {
			baserow = this.categoryTbl;
		}
		if (name.equals("accessory")) {
			baserow = this.accessoryTbl;
		}
		return baserow;
	}

	public Integer insertTable(String name, BaseRow row) {
		int i = 0;
		getTable(name).add(row);
		return i;
	}

	public ArrayList<BaseRow> selectTable(String name) {
		return getTable(name);
	}

	public Integer updateTable(String name, BaseRow oldRow, BaseRow newRow) {
		int i = 0;
		ArrayList<BaseRow> objEnitity = selectTable(name);
		Integer index = objEnitity.indexOf(oldRow);
		getTable(name).set(index, newRow);
		i = 1;
		return i;
	}

	public boolean deleteTable(String name, BaseRow row) {
		boolean check = false;
		check = getTable(name).remove(row);
		return check;
	}

	public void truncateTable(String name) {
		getTable(name).clear();
		System.out.println("Truncate product");
	}

	public Integer updateTable2(String name, int id, BaseRow row) {
		int check = 0;
		ArrayList<BaseRow> listEntity = selectTable(name);
		BaseRow baseRow = listEntity.stream().filter(baseRows -> id == baseRows.getId()).findAny().orElse(null);
		Integer index = listEntity.indexOf(baseRow);
		getTable(name).set(index, row);
		check = 1;
		return check;
	}

}
