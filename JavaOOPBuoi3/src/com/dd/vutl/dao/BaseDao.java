package com.dd.vutl.dao;

import java.util.ArrayList;

import com.dd.vutl.entity.BaseRow;

public abstract class BaseDao {
	public abstract Integer insert(BaseRow row);
	public abstract Integer update(BaseRow oldRow,BaseRow newRow);
	public abstract boolean delete(BaseRow row);
	public abstract ArrayList<BaseRow> findAll();
	public abstract BaseRow findById(int id);
	public abstract BaseRow findByName(String name);
}

