package com.dd.vutl.dao;

import java.util.ArrayList;

import com.dd.vutl.entity.BaseRow;

public class CategoryDAO extends BaseDao {

	static Database database = new Database();
	
	@Override
	public Integer insert(BaseRow row) {
		// TODO Auto-generated method stub
		int i;
		i = database.insertTable("category", row);
		return i;
	}

	@Override
	public Integer update(BaseRow oldRow, BaseRow newRow) {
		// TODO Auto-generated method stub
		int i;
		i = database.updateTable("category", oldRow, newRow);
		return i;
	}

	@Override
	public boolean delete(BaseRow row) {
		// TODO Auto-generated method stub
		boolean check;
		check = database.deleteTable("category", row);
		return check;
	}

	@Override
	public ArrayList<BaseRow> findAll(){
		ArrayList<BaseRow> listCategory = database.selectTable("category");
		return listCategory ;
		
	}

	@Override
	public BaseRow findById(int id) {
		// TODO Auto-generated method stub
		ArrayList<BaseRow> listCategory = database.selectTable("category");
		BaseRow categorys = listCategory.stream().filter(category -> id == category.getId()).findAny().orElse(null);
		return categorys;
	}

	@Override
	public BaseRow findByName(String name) {
		// TODO Auto-generated method stub
		ArrayList<BaseRow> listCategory = database.selectTable("category");
		BaseRow categorys = listCategory.stream().filter(category -> name == category.getName()).findAny().orElse(null);
		return categorys;
	}
	
//	public Integer insert(Object row) {
//		int i;
//		i = database.insertTable("category", row);
//		return i;
//	}
//
//	public Integer update(Object oldRow, Object newRow) {
//		int i;
//		i = database.updateTable("category", oldRow, newRow);
//		return i;
//	}
//
//	public boolean delete(Object row) {
//		boolean check;
//		check = database.deleteTable("category", row);
//		return check;
//	}
//
//	public ArrayList<Object> findAll() {
//		ArrayList<Object> listCategory = database.selectTable("category");
//		return listCategory;
//	}
//
//	public static Object findByID(int id) {
//		ArrayList<Object> listCategory = database.selectTable("category");
//		Category category1 = new Category();
//		for (int i = 0; i < listCategory.size(); i++) {
//			Category category2 = (Category) listCategory.get(i);
//			if (id == category2.getId()) {
//				category1 = category2;
//			}
//		}
//		return category1;
//	}
//
//	public static Object findByName(String name) {
//		ArrayList<Object> listCategory = database.selectTable("category");
//		Category category1 = new Category();
//		for (int i = 0; i < listCategory.size(); i++) {
//			Category category2 = (Category) listCategory.get(i);
//			if (name == category2.getName()) {
//				category1 = category2;
//			}
//		}
//		return category1;
//	}
}
