package com.dd.vutl.dao;

import java.util.ArrayList;

import com.dd.vutl.entity.BaseRow;

public class AccessoryDAO extends BaseDao {
	
	static Database database = new Database();
	
	@Override
	public Integer insert(BaseRow row) {
		// TODO Auto-generated method stub
		int i;
		i = database.insertTable("accessory", row);
		return i;
	}

	@Override
	public Integer update(BaseRow oldRow, BaseRow newRow) {
		// TODO Auto-generated method stub
		int i;
		i = database.updateTable("accessory", oldRow, newRow);
		return i;
	}

	@Override
	public boolean delete(BaseRow row) {
		// TODO Auto-generated method stub
		boolean check;
		check = database.deleteTable("accessory", row);
		return check;
	}

	@Override
	public ArrayList<BaseRow> findAll(){
		ArrayList<BaseRow> listAccessory = database.selectTable("accessory");
		return listAccessory ;
		
	}

	@Override
	public BaseRow findById(int id) {
		// TODO Auto-generated method stub
		ArrayList<BaseRow> listAccessory = database.selectTable("accessory");
		BaseRow accessorys = listAccessory.stream().filter(accessory -> id == accessory.getId()).findAny().orElse(null);
		return accessorys;
	}

	@Override
	public BaseRow findByName(String name) {
		// TODO Auto-generated method stub
		ArrayList<BaseRow> listAccessory = database.selectTable("accessory");
		BaseRow accessorys = listAccessory.stream().filter(accessory -> name.equals(accessory.getName())).findAny()
				.orElse(null);

		return accessorys;
	}

//	public Integer insert(Object row) {
//		int i;
//
//		i = database.insertTable("accessory", row);
//		return i;
//	}
//
//	public Integer update(Object oldRow, Object newRow) {
//		int i;
//		i = database.updateTable("accessory", oldRow, newRow);
//		return i;
//	}
//
//	public boolean delete(Object row) {
//		boolean check;
//		check = database.deleteTable("accessory", row);
//		return check;
//	}
//
//	public ArrayList<Object> findAll() {
//		ArrayList<Object> listAccessory = database.selectTable("accessory");
//		return listAccessory;
//	}
//
//	public static Object findByID(int id) {
//		ArrayList<Object> listAccessory = database.selectTable("accessory");
//		Accessory accessory1 = new Accessory();
//		for (int i = 0; i < listAccessory.size(); i++) {
//			Accessory accessory2 = (Accessory) listAccessory.get(i);
//			if (id == accessory2.getId()) {
//				accessory1 = accessory2;
//			}
//		}
//		return accessory1;
//	}
//	public static Object findByName(String name) {
//		ArrayList<Object> listAccessory = database.selectTable("accessory");
//		Accessory accessory1 = new Accessory();
//		for (int i = 0; i < listAccessory.size(); i++) {
//			Accessory accessory2 = (Accessory) listAccessory.get(i);
//			if (name == accessory2.getName()) {
//				accessory1 = accessory2;
//			}
//		}
//		return accessory1;
//	}
}
