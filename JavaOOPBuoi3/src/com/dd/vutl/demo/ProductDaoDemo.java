package com.dd.vutl.demo;

import java.util.ArrayList;

import com.dd.vutl.dao.Database;
import com.dd.vutl.dao.ProductDAO;
import com.dd.vutl.entity.BaseRow;
import com.dd.vutl.entity.Product;

public class ProductDaoDemo {
	static Database database = new Database();
	static ProductDAO productDao = new ProductDAO();
	
	public static void main(String[] args) {
		insertProductTest();
		updateProductTest();
		deleteProductTest();
		findAllTest();
		System.out.println("Find by ID:");
		findByIDTest();
		System.out.println("Find by Name:");
		findByNameTest();
	}

	public static Integer insertProductTest() {
		Product product = new Product(1, "Product1");
		Product product1 = new Product(2, "Product2");
		Product product2 = new Product(3, "Product3");
		Product product3 = new Product(4, "Product4");
		int check;
		if (productDao.insert(product) == 1 && productDao.insert(product1) == 1 && productDao.insert(product2) == 1
				&& productDao.insert(product3) == 1) {
			check = 1;
			System.out.println("Insert Success");
		} else {
			check = 1;
			System.out.println("Insert Fail");
		}
		return check;
	}
	
	public static Integer updateProductTest() {
		ArrayList<BaseRow> listProducts = productDao.findAll();
		BaseRow oldProduct = listProducts.get(1);
		Product newProduct = new Product(8,"Product 8");
		int i = productDao.update(oldProduct, newProduct);
		if(i==1) {
			System.out.println("Update Success");
		}else {
			i = 0;
			System.out.println("Update Fail");
		}
		return i;

	}
	
	public static boolean deleteProductTest() {
		ArrayList<BaseRow> listProducts = productDao.findAll();
		BaseRow product = listProducts.get(0);
		boolean check = productDao.delete(product);
		System.out.println("Delete : "+product+" "+check);
		return check;

	}
	
	public static void findAllTest() {
		ArrayList<BaseRow> listProducts = productDao.findAll();
		System.out.println(listProducts);
	}
	
	public static void findByIDTest() {
		BaseRow product = productDao.findById(4);
		System.out.println(product);
	}
	
	public static void findByNameTest() {
		BaseRow product = productDao.findByName("Product3");
		System.out.println(product);
	}

}
