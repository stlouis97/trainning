package com.dd.vutl.demo;

import java.util.ArrayList;

import com.dd.vutl.dao.Database;
import com.dd.vutl.entity.BaseRow;
import com.dd.vutl.entity.Category;

public class CategoryDemo {
	static Database database = new Database();
	
	public static void main(String[] args) {
		createCategoryTest();
		selectCategoryTest();
		printCategory();
	}

	public static void createCategoryTest() {

		Category category = new Category(1, "category1");
		Category category2 = new Category(2, "category2");
		Category category3 = new Category(3, "category3");
		Category category4 = new Category(4, "category4");
		Category category5 = new Category(5, "category5");
		database.insertTable("category", category);
		database.insertTable("category", category2);
		database.insertTable("category", category3);
		database.insertTable("category", category4);
		database.insertTable("category", category5);
	}

	public static void selectCategoryTest() {
		ArrayList<BaseRow> category = database.selectTable("category");
		System.out.println(category);
	}

	public static void printCategory() {
		System.out.println(new Category(1, "category1"));

	}
}
