package com.dd.vutl.demo;

import java.util.ArrayList;

import com.dd.vutl.dao.Database;
import com.dd.vutl.entity.Accessory;
import com.dd.vutl.entity.BaseRow;
import com.dd.vutl.entity.Category;
import com.dd.vutl.entity.Product;

public class DatabaseDemo {
	static Database database = new Database();

	public static void main(String[] args) {
		insertTableTest();
		System.out.println("=====");
		selectTableTest();
		System.out.println("=====");
		truncateTableTest();
		System.out.println("=====");
		initDatabaseTest();
		System.out.println("=====");
		printTableTest();
		System.out.println("=====");
		updateTableTest();
		System.out.println("=====");
		updateTableTest2();
		System.out.println("=====");
		deleteTableTest();
	}

	private static void insertTableTest() {
		Product product = new Product(1, "Product1");
		Product product2 = new Product(2, "Product2");
		database.insertTable("product", product);
		database.insertTable("product", product2);
		Category category = new Category(1, "Category1");
		Category category2 = new Category(2, "Category2");
		database.insertTable("category", category);
		database.insertTable("category", category2);
		Accessory accessory = new Accessory(1, "Accessory1");
		Accessory accessory1 = new Accessory(2, "Accessory2");
		database.insertTable("accessory", accessory);
		database.insertTable("accessory", accessory1);
	}

	private static void selectTableTest() {
		ArrayList<BaseRow> products = database.selectTable("product");
		System.out.println("Select :" + products);
		ArrayList<BaseRow> categorys = database.selectTable("category");
		System.out.println("Select :" + categorys);
		ArrayList<BaseRow> accessorys = database.selectTable("accessory");
		System.out.println("Select :" + accessorys);
	}

	private static void updateTableTest() {
		ArrayList<BaseRow> category = database.selectTable("category");
		BaseRow oldCategory = category.get(1);
		Category newCategory = new Category(3, "Update Category");
		database.updateTable("category", oldCategory, newCategory);
		System.out.println("Old Category : " + oldCategory + " --> " + "New Category : " + newCategory);
		ArrayList<BaseRow> product = database.selectTable("product");
		BaseRow oldProduct = product.get(1);
		Product newProduct = new Product(3, "Update Product");
		database.updateTable("product", oldProduct, newProduct);
		System.out.println("Old Product : " + oldProduct + " --> " + "New Product : " + newProduct);
		ArrayList<BaseRow> accessory = database.selectTable("accessory");
		BaseRow oldAccessory = accessory.get(0);
		Accessory newAccessory = new Accessory(3, "Update Accessory");
		database.updateTable("accessory", oldAccessory, newAccessory);
		System.out.println("Old Accessory : " + oldAccessory + " --> " + "New Accessory : " + newAccessory);
	}

	private static void updateTableTest2() {
		Product product = new Product(9, "New Update Product");
		database.updateTable2("product", 1, product);
		Category category = new Category(9, " New Update Category");
		database.updateTable2("category",3 ,category);
		Accessory accessory = new Accessory(9, " New Update Accessory");
		database.updateTable2("accessory", 2, accessory);
	}

	private static boolean deleteTableTest() {
		ArrayList<BaseRow> objCategory = database.selectTable("category");
		BaseRow category = objCategory.get(0);
		Boolean isDelete = database.deleteTable("category", category);
		System.out.println("Delete : " + category + " Trang Thai : " + isDelete);
		return isDelete;
	}

	private static void truncateTableTest() {
		database.truncateTable("product");
	}

	private static void initDatabaseTest() {
		// Tao list Product
		Accessory accessory = new Accessory(1, "Accessory");
		Accessory accessory2 = new Accessory(2, "Accessory2");
		Accessory accessory3 = new Accessory(3, "Accessory3");
		Accessory accessory4 = new Accessory(4, "Accessory4");
		Accessory accessory5 = new Accessory(5, "Accessory5");
		database.insertTable("accessory", accessory);
		database.insertTable("accessory", accessory2);
		database.insertTable("accessory", accessory3);
		database.insertTable("accessory", accessory4);
		database.insertTable("accessory", accessory5);
		Category category1 = new Category(1, "Category1");
		Category category2 = new Category(2, "Category2");
		Category category3 = new Category(3, "Category3");
		Category category4 = new Category(4, "Category4");
		Category category5 = new Category(5, "Category5");
		database.insertTable("category", category1);
		database.insertTable("category", category2);
		database.insertTable("category", category3);
		database.insertTable("category", category4);
		database.insertTable("category", category5);
		Product product = new Product(1, "Product1");
		Product product2 = new Product(2, "Product2");
		Product product3 = new Product(3, "Product3");
		Product product4 = new Product(4, "Product4");
		Product product5 = new Product(5, "Product5");
		database.insertTable("product", product);
		database.insertTable("product", product2);
		database.insertTable("product", product3);
		database.insertTable("product", product4);
		database.insertTable("product", product5);
	}

	private static void printTableTest() {
		System.out.println("Print Table : ");
		ArrayList<BaseRow> products = database.selectTable("product");
		System.out.println(products);
		ArrayList<BaseRow> categorys = database.selectTable("category");
		System.out.println(categorys);
		ArrayList<BaseRow> accessorys = database.selectTable("accessory");
		System.out.println(accessorys);
	}

}
