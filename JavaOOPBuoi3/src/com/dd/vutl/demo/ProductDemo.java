package com.dd.vutl.demo;

import java.util.ArrayList;

import com.dd.vutl.dao.Database;
import com.dd.vutl.entity.BaseRow;
import com.dd.vutl.entity.Product;

public class ProductDemo {
	static Database database = new Database();

	public static void main(String[] args) {
		createProductTest();
		selectProductTest();
		printProduct();
	}

	public static void createProductTest() {

		Product product = new Product(1, "Audi");
		Product product2 = new Product(2, "BMW");
		Product product3 = new Product(3, "Toyota");
		Product product4 = new Product(4, "Ferrari");
		Product product5 = new Product(5, "Lamborghini");
		database.insertTable("product", product);
		database.insertTable("product", product2);
		database.insertTable("product", product3);
		database.insertTable("product", product4);
		database.insertTable("product", product5);
	}

	public static void selectProductTest() {
		ArrayList<BaseRow> products = database.selectTable("product");
		System.out.println(products);
	}

	public static void printProduct() {
		System.out.println(new Product(1, "Audi"));

	}

}
