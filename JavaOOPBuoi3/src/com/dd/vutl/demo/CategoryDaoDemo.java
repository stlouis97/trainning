package com.dd.vutl.demo;

import java.util.ArrayList;

import com.dd.vutl.dao.CategoryDAO;
import com.dd.vutl.dao.Database;
import com.dd.vutl.entity.BaseRow;
import com.dd.vutl.entity.Category;

public class CategoryDaoDemo {
	static Database database = new Database();
	static CategoryDAO categoryDao = new CategoryDAO();

	public static void main(String[] args) {
		insertCategoryTest();
		updateCategoryTest();
		deleteCategoryTest();
		findAllTest();
		System.out.println("Find by ID:");
		findByIDTest();
		System.out.println("Find by Name:");
		findByNameTest();
	}

	public static Integer insertCategoryTest() {
		Category category = new Category(1, "Category1");
		Category category2 = new Category(2, "Category2");
		Category category3 = new Category(3, "Category3");
		Category category4 = new Category(4, "Category3");
		Category category5 = new Category(5, "Category5");
		int check;
		if (categoryDao.insert(category) == 1 && categoryDao.insert(category2) == 1
				&& categoryDao.insert(category3) == 1 && categoryDao.insert(category4) == 1 && categoryDao.insert(category5) == 1) {
			check = 1;
			System.out.println("Insert Success");
		} else {
			check = 1;
			System.out.println("Insert Fail");
		}
		return check;
	}

	public static Integer updateCategoryTest() {
		ArrayList<BaseRow> listcategory = categoryDao.findAll();
		BaseRow oldcategory = listcategory.get(1);
		Category newcategory = new Category(8,"category 8");
		int i = categoryDao.update(oldcategory, newcategory);
		if(i==1) {
			System.out.println("Update Success");
		}else {
			i = 0;
			System.out.println("Update Fail");
		}
		return i;

	}

	public static boolean deleteCategoryTest() {
		ArrayList<BaseRow> listcategory = categoryDao.findAll();
		BaseRow category = listcategory.get(3);
		boolean check = categoryDao.delete(category);
		System.out.println("Delete : "+category+" "+check);
		return check;

	}
	
	public static void findAllTest() {
		ArrayList<BaseRow> listcategory= categoryDao.findAll();
		System.out.println(listcategory);
	}
	
	public static void findByIDTest() {
		BaseRow category = categoryDao.findById(3);
		System.out.println(category);
	}
	
	public static void findByNameTest() {
		BaseRow category = categoryDao.findByName("Category5");
		System.out.println(category);
	}
}
