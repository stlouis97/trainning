package com.dd.vutl.Bai12;

public class Queue {

	private Object[] queueArray;
	private int top;
	private int min;
	private int maxSize;

	public Queue(int max) {
		maxSize = max;
		queueArray = new Object[maxSize];
		top = -1;
		min = 0;
	}


	public void print(Queue queue) {
		for (int i = min; i <= queue.top; i++) {
			System.out.println(queueArray[i]);
		}
	}

	public void push(Object object) {
		try {
			queueArray[++top] = object;
		} catch (Exception e) {
			System.out.println("Mảng đã đầy");
		}
	}

	public Object get() {
		Object obj = new Object();
		try {
			obj = queueArray[min++];
		}catch (ArrayIndexOutOfBoundsException e) {
			// TODO: handle exception
			System.out.println("Mảng trống");
		}
		return obj;
	}


	public static void main(String[] args) throws Exception {
		Queue queue = new Queue(7);
		queue.push(new Product("CPU", 750, 10, 1));
		queue.push(new Product("RAM", 50, 2, 2));
		queue.push(new Product("HDD", 70, 1, 2));
		queue.push(new Product("Main", 400, 3, 1));
		queue.push(new Product("Keyboard", 30, 8, 4));
		queue.push(new Product("Mouse", 25, 50, 4));
		queue.push(new Product("VGA", 60, 35, 3));
		queue.get();
		queue.get();
		queue.get();
		queue.print(queue);

		

	}

}
