package com.dd.vutl.Bai12;

import java.util.ArrayList;
import java.util.List;

public class Bai17Selection {

	static ArrayList<Product> listProducts = Product.addProductList();

	public static void main(String[] args) {
		List<Product> listProductSortByName = sortByName(listProducts);
		System.out.println(listProductSortByName);

	}

	public static List<Product> sortByName(List<Product> listProduct) {
		for (int i = 0; i < listProduct.size() - 1; i++)  
        {  
            int index = i;  
            for (int j = i + 1; j < listProduct.size(); j++){  
                if (listProduct.get(j).getName().length() > listProduct.get(index).getName().length()){  
                    index = j;//searching for lowest index  
                }  
            }
            Product temp = listProduct.get(index);
			listProduct.set(index, listProduct.get(i));
			listProduct.set(i, temp);
        }
		return listProducts;  
	}
}
