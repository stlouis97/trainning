package com.dd.vutl.Bai12;

import java.util.ArrayList;
import java.util.List;

public class Bai15 {
	static ArrayList<Product> listProduct = Product.addProductList();
	static ArrayList<Category> listCategory = Category.addCategoryList();

	public static void main(String[] args) {
		List<Product> listMapProductByCategory = mapProductByCategory(listProduct, listCategory);
		System.out.println(listMapProductByCategory);
	}

	public static List<Product> mapProductByCategory(ArrayList<Product> listProduct, ArrayList<Category> listCategory) {
		List<Product> products = new ArrayList<Product>();
		for (Product product : listProduct) {
			for (Category category : listCategory) {
				if (product.getCategoryID() == category.getId()) {
					product.setCategoryName(category.getName());
				}
				
			}
			products.add(product);
		}
		return products;

	}
}

