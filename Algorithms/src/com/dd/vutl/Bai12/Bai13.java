package com.dd.vutl.Bai12;

import java.util.ArrayList;
import java.util.List;

public class Bai13 {
	static ArrayList<Product> listProduct = Product.addProductList();

	public static void main(String[] args) {
		List<Product> listProducts = findProductByCategory(listProduct, 2);
		System.out.println(listProducts);
	}

	public static List<Product> findProductByCategory(List<Product> listProduct, int categoryID) {
		List<Product> products = new ArrayList<Product>();
		for (Product product : listProduct) {
			if (product.getCategoryID() == categoryID) {
				products.add(product);
				System.out.println(product);
			}
		}
	return products;
	}
}
