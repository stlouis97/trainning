package com.dd.vutl.Bai12;

import java.util.ArrayList;
import java.util.List;

public class Bai17Insertion {
	
	static ArrayList<Product> listProducts = Product.addProductList();

	public static void main(String[] args) {
		List<Product> listProductSortByPrice = sortByPrice(listProducts);
		System.out.println(listProductSortByPrice);

	}

	public static List<Product> sortByPrice(List<Product> listProduct) {
		List<Product> products = listProduct;
		for (int j = 1; j < listProduct.size(); j++) {
			Product temp = listProduct.get(j);
			int i = j - 1;
			while ((i > -1) && (products.get(i).getName().length() < temp.getName().length())) {
				products.set(i + 1, listProduct.get(i));
				i = i - 1;
			}
			products.set(i + 1, temp);
		}
		return products;
	}
}
