package com.dd.vutl.Bai12;

import java.util.ArrayList;

public class Category {
	public int id;
	public String name;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Category(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	@Override
	public String toString() {
		return "Category [id=" + id + ", name=" + name + "]";
	}	
	
	public static ArrayList<Category> addCategoryList() {
		ArrayList<Category> listCategory = new ArrayList<>();
		listCategory.add(new Category(1, "Computer"));
		listCategory.add(new Category(2, "Memory"));
		listCategory.add(new Category(3, "Card"));
		listCategory.add(new Category(4, "Accessory"));
		return listCategory;
		
	}
	
}
