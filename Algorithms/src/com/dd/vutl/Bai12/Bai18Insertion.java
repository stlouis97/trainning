package com.dd.vutl.Bai12;

import java.util.ArrayList;
import java.util.List;

public class Bai18Insertion {
	
	static ArrayList<Product> listProducts = Product.addProductList();
	static ArrayList<Category> listCategory = Category.addCategoryList();
	
	public static void main(String[] args) {
		List<Product> listMapProductByCategory = mapProductByCategory(listProducts, listCategory);
		System.out.println(listMapProductByCategory);
		List<Product> listProductSortByCategory = sortByCategoryName(listProducts);
		System.out.println(listProductSortByCategory);

	}
	
	public static List<Product> mapProductByCategory(ArrayList<Product> listProduct, ArrayList<Category> listCategory) {
		List<Product> products = new ArrayList<Product>();
		for (Product product : listProduct) {
			for (Category category : listCategory) {
				if (product.getCategoryID() == category.getId()) {
					product.setCategoryName(category.getName());
				}
				
			}
			products.add(product);
		}
		return products;

	}
	
	public static List<Product> sortByCategoryName(List<Product> listProduct) {
		List<Product> products = listProduct;
		for (int j = 1; j < listProduct.size(); j++) {
			Product temp = listProduct.get(j);
			int i = j - 1;
			while ((i > -1) && (listProduct.get(i).getCategoryName().compareTo(temp.getCategoryName())>0)) {
				products.set(i + 1, listProduct.get(i));
				i = i - 1;
			}
			products.set(i + 1, temp);
		}
		return products;
	}
}
