package com.dd.vutl.Bai12;

import java.util.ArrayList;
import java.util.List;

public class Bai17Bubble {
	static ArrayList<Product> listProducts = Product.addProductList();

	public static void main(String[] args) {
		List<Product> listProductSortByName = sortByName(listProducts);
		System.out.println(listProductSortByName);

	}

	public static List<Product> sortByName(List<Product> listProduct) {
		int i, j;
		for (i = 0; i < listProduct.size() - 1; i++) {
			for (j = i + 1; j < listProduct.size(); j++) {
				if (listProduct.get(i).getName().length() < listProduct.get(j).getName().length()) {
					Product temp = listProduct.get(j);
					listProduct.set(j, listProduct.get(i));
					listProduct.set(i, temp);
				}
			}
		}
		return listProducts;
	}
}
