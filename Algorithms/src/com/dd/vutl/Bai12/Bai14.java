package com.dd.vutl.Bai12;

import java.util.ArrayList;
import java.util.List;

public class Bai14 {
	static ArrayList<Product> listProduct = Product.addProductList();

	public static void main(String[] args) {
		List<Product> listProducts = findProductByPrice(listProduct, 70);
		System.out.println(listProducts);
	}

	public static List<Product> findProductByPrice(List<Product> listProduct, int price) {
		List<Product> products = new ArrayList<Product>();
			for (Product product : listProduct) {
				if (product.getPrice() <= price) {
					products.add(product);
					System.out.println(product);
				}
			}
		return products;
	}
}
