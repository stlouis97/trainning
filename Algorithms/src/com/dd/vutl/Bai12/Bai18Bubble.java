package com.dd.vutl.Bai12;

import java.util.ArrayList;
import java.util.List;

public class Bai18Bubble {
	static ArrayList<Product> listProducts = Product.addProductList();
	static ArrayList<Category> listCategory = Category.addCategoryList();

	public static void main(String[] args) {
		List<Product> listMapProductByCategory = mapProductByCategory(listProducts, listCategory);
		System.out.println(listMapProductByCategory);
		List<Product> listProductSortByCategory = sortByCategoryName(listProducts);
		System.out.println(listProductSortByCategory);

	}
	
	public static List<Product> mapProductByCategory(ArrayList<Product> listProduct, ArrayList<Category> listCategory) {
		List<Product> products = new ArrayList<Product>();
		for (Product product : listProduct) {
			for (Category category : listCategory) {
				if (product.getCategoryID() == category.getId()) {
					product.setCategoryName(category.getName());
				}
				
			}
			products.add(product);
		}
		return products;

	}
	
	public static List<Product> sortByCategoryName(List<Product> listProduct) {
		for (int i = 0; i < listProduct.size() - 1; i++) {
			for (int j = i + 1; j < listProduct.size(); j++) {
				if (listProduct.get(i).getCategoryName().compareTo(listProduct.get(j).getCategoryName())>0) {
					Product temp = listProduct.get(i);
					listProduct.set(i, listProduct.get(j));
					listProduct.set(j, temp);
				}
			}
		}
		return listProducts;
	}
}
