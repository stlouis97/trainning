package com.dd.vutl.Bai12;

public class Bai21 {

	private static int maxSize = 10;
	private static Object[] queueArray = new Object[maxSize];
	private static int front = 0;
	private static int rear = -1;
	private static int itemCount = 0;

	public static Object peek() {
		return queueArray[front];
	}

	public static boolean isEmpty() {
		return itemCount == 0;
	}

	public static boolean isFull() {
		return itemCount == maxSize;
	}

	int size() {
		return itemCount;
	}

	public static void push(Object data) {
		if (!isFull()) {
			if (rear == maxSize - 1) {
				rear = -1;
			}
			queueArray[++rear] = data;
			itemCount++;
		}
	}

	public static Object get() {
		Object data = queueArray[front++];

		if (front == maxSize) {
			front = 0;
		}

		itemCount--;
		return data;
	}

	public static void main(String[] args) throws Exception {
		push(new Product("CPU", 750, 10, 1));
		push(new Product("RAM", 50, 2, 2));
		push(new Product("HDD", 70, 1, 2));
		push(new Product("Main", 400, 3, 1));
		push(new Product("Keyboard", 30, 8, 4));
		push(new Product("Mouse", 25, 50, 4));
		push(new Product("VGA", 60, 35, 3));

		if (isFull()) {
			System.out.println("Hàng đợi đã đầy");
		}
		Object dau = peek();
		System.out.println("Phần tử đầu tiên của hàng đợi: " + dau);
		Object element = get();
		System.out.println("Phần tử bị xóa: " + element);
		System.out.println("Hàng đợi hiện tại:  ");
		
		while (!isEmpty()) {
			Object n = get();
			System.out.println(" " + n);
		}

	}

}
