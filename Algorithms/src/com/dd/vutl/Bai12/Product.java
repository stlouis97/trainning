package com.dd.vutl.Bai12;

import java.util.ArrayList;

public class Product {
	public String name;
	public int price;
	public int quality;
	public int categoryID;
	public String categoryName;

	public Product(String name, int price, int quality, int categoryID) {
		this.name = name;
		this.price = price;
		this.quality = quality;
		this.categoryID = categoryID;
	}
	public Product() {
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getQuality() {
		return quality;
	}

	public void setQuality(int quality) {
		this.quality = quality;
	}

	public int getCategoryID() {
		return categoryID;
	}

	public void setCategoryID(int categoryID) {
		this.categoryID = categoryID;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	@Override
	public String toString() {
		return "Product [name=" + name + ", price=" + price + ", quality=" + quality + ", categoryID=" + categoryID
				+ ", categoryName=" + categoryName + "]";
	}

	public Product(String name, int price, int quality, int categoryID, String categoryName) {
		super();
		this.name = name;
		this.price = price;
		this.quality = quality;
		this.categoryID = categoryID;
		this.categoryName = categoryName;
	}

	public static ArrayList<Product> addProductList() {
		ArrayList<Product> listProduct = new ArrayList<>();
		listProduct.add(new Product("CPU", 750, 10, 1));
		listProduct.add(new Product("RAM", 50, 2, 2));
		listProduct.add(new Product("HDD", 70, 1, 2));
		listProduct.add(new Product("Main", 400, 3, 1));
		listProduct.add(new Product("Keyboard", 30, 8, 4));
		listProduct.add(new Product("Mouse", 25, 50, 4));
		listProduct.add(new Product("VGA", 60, 35, 3));
		listProduct.add(new Product("Monitor", 120, 28, 2));

		System.out.println(listProduct);
		return listProduct;
	}
	

}
