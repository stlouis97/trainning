package com.dd.vutl.Bai12;

public class Stack {

	private int maxSize;
	private Object[] stackArray;
	private int top;

	public Stack(int s) {
		maxSize = s;
		stackArray = new Object[maxSize];
		top = -1;
	}

	public void push(Object j) throws Exception {
		stackArray[++top] = j;
	}

	public Object get() {
		return stackArray[top--];
	}

	public Object peek() {
		return stackArray[top];
	}


	// in cac phan tu cua stack
	public void Output(Stack S) {
		for (int i = S.top; i >= 0; i--) {
			System.out.println(" " + stackArray[i]);
		}
	}

	public static void main(String[] args) throws Exception {
		Stack myStack = new Stack(10);
		myStack.push(new Product("CPU", 750, 10, 1));
		myStack.push(new Product("RAM", 50, 2, 2));
		myStack.push(new Product("HDD", 70, 1, 2));
		myStack.push(new Product("Main", 400, 3, 1));
		myStack.push(new Product("Keyboard", 30, 8, 4));
		myStack.push(new Product("Mouse", 25, 50, 4));
		myStack.push(new Product("VGA", 60, 35, 3));
		System.out.println("List stack :");
		myStack.Output(myStack);
		Object value = myStack.get();
		System.out.println("Xóa phần tử đấu tiên: " + value);
		System.out.println("List stack after remove :");
		myStack.Output(myStack);

	}

}
