package com.dd.vutl.Algorithms;

public class Bai20 {
	
	public static void main (String[] args) {
		double i = calMonth(1000, 0.05);
		System.out.println(i);
	}
	
	public static Integer calMonth(double tienGui, double rate) {
		int month = 0;
		double tienTK = tienGui + tienGui;
		while (tienGui <= tienTK) {
			month++;
			double tienLai = tienGui * rate;
			tienGui = tienGui + tienLai;
		}
		return month;
	}
	
}
