package com.dd.vutl.Algorithms;

import java.util.LinkedList;
import java.util.Queue;

public class Bai11 {
	public static void main(String[] args) {
		Queue<Integer> q = new LinkedList<>();

		// Adds elements {0, 1, 2, 3, 4} to queue
		for (int i = 0; i < 6; i++)
			q.add(i);

		// Display contents of the queue.
		System.out.println("Các giá trị của hàng chờ-" + q);

		// To remove the head of queue.
		int removedele = q.remove();
		System.out.println("Xóa giá trị-" + removedele);

		System.out.println(q);

		// To view the head of queue
		int head = q.peek();
		System.out.println("Đầu hàng chờ-" + head);

		// Rest all methods of collection interface,
		// Like size and contains can be used with this
		// implementation.
		int size = q.size();
		System.out.println("Kích thước hàng chờ-" + size);
	}
}
