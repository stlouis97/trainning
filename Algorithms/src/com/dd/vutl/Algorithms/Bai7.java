package com.dd.vutl.Algorithms;

public class Bai7 {
	
	static int arrayInt[] = { 23, 17, 14, 29, 2, 31, 22 };
	
	public static void main(String[] args) {
		selectionSort(arrayInt);
	}
	
	public static void selectionSort(int[] arr) {

		for (int i = 0; i < arr.length - 1; i++)  
        {  
            int index = i;  
            for (int j = i + 1; j < arr.length; j++){  
                if (arr[j] < arr[index]){  
                    index = j;//searching for lowest index  
                }  
            }  
            int smallerNumber = arr[index];   
            arr[index] = arr[i];  
            arr[i] = smallerNumber;  
        }  
		for(int i:arrayInt){  
            System.out.print(i+" ");  
        }  
	}
}
