package com.dd.vutl.Algorithms;

public class Bai10 {
	private int maxSize;
	private long[] stackArray;
	private int top;

	public Bai10(int s) {
	      maxSize = s;
	      stackArray = new long[maxSize];
	      top = -1;
	   }

	public void push(long j) {
		stackArray[++top] = j;
	}

	public long get() {
		return stackArray[top--];
	}

	public long peek() {
		return stackArray[top];
	}

	public boolean isEmpty() {
		return (top == -1);
	}

	public boolean isFull() {
		return (top == maxSize - 1);
	}

	public static void main(String[] args) {
		Bai10 theStack = new Bai10(10);
		theStack.push(10);
		theStack.push(20);
		theStack.push(40);
		theStack.push(50);
		theStack.push(30);
		
		while (!theStack.isEmpty()) {
			long value = theStack.get();
			System.out.print(value);
			System.out.print(" ");
		}
		System.out.println("");
	}

}
