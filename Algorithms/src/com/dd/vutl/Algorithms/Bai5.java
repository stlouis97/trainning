package com.dd.vutl.Algorithms;

public class Bai5 {
	static int arrayInt[] = { 23, 17, 14, 29, 2, 31, 22 };

	public static void main(String[] args) {
		bubbleSort(arrayInt);

	}

	public static void bubbleSort(int[] arr) {
		System.out.println("Sort by Bubble Sort:");
		for(int i = 0; i< arr.length; i++){
            for (int j = arr.length - 1; j > 0; j--) {
               if(arr[j] < arr[j-1]){
                   int temp = arr[j];
                   arr[j] = arr[j-1];
                   arr[j-1] = temp;
               }
            }
            System.out.println(" " + arrayInt[i]);
        }

	}
}
