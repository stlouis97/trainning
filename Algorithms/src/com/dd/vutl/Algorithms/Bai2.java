package com.dd.vutl.Algorithms;

public class Bai2 {
	static int arrayInt[] = { 23, 17, 14, 29, 2, 31, 22 };

	public static void main(String[] args) {
		printArray(arrayInt);
		findArrayByElement(arrayInt, 17);
		findArrayByIndex(arrayInt, 2);
		insertArray(arrayInt, 87);
		deleteByIndex(arrayInt, 0);
		deleteByElement(arrayInt, 14);
		updateArrayByIndex(arrayInt, 0, 99);
		updateArrayByElement(arrayInt, 29, 83);
	}

	public static void printArray(int[] arr) {
		System.out.println("Array Length : " + arr.length);
		for (int i = 0; i < arr.length; i++) {

			System.out.println(" " + arrayInt[i]);
		}
	}

	public static void insertArray(int[] arr, int element) {
		System.out.println("Push to Array : ");
		int newArr[] = new int[arr.length + 1];
		for (int i = 0; i < arr.length; i++) {
			newArr[i] = arr[i];
		}
		newArr[arr.length] = element;
		for (int i = 0; i < newArr.length; i++) {
			System.out.println(" " + newArr[i]);
		}
	}

	public static void deleteByIndex(int[] arr, int index) {
		int i, j;
		System.out.println("Delete by Index of Array : ");
		int newArr[] = new int[arr.length - 1];
		int deleteArr[] = new int[1];
		for (j = i = 0; i < arr.length; i++) {
			if (i != index) {
				newArr[j] = arr[i];
				j++;
			} else {
				deleteArr[0] = arr[i];
			}
		}
		for (int c = 0; c < newArr.length; c++) {
			System.out.println(" " + newArr[c]);
		}
		System.out.println("Delete : " + deleteArr[0]);
	}

	public static void deleteByElement(int[] arr, int element) {
		int i, j;
		System.out.println("Delete by Element of Array : ");
		int newArr[] = new int[arr.length - 1];
		int deleteArr[] = new int[1];
		for (j = i = 0; i < arr.length; i++) {
			if (arr[i] != element) {
				newArr[j] = arr[i];
				j++;
			} else {
				deleteArr[0] = arr[i];
			}
		}
		for (int c = 0; c < newArr.length; c++) {
			System.out.println(" " + newArr[c]);
		}
		System.out.println("Delete : " + deleteArr[0]);
	}

	public static void findArrayByElement(int[] arr, int element) {
		int i;
		int newArr[] = new int[1];
		for (i = 0; i < arr.length; i++) {
			if (arr[i] == element) {
				newArr[0] = arr[i];
			}
		}
		System.out.println("Find by Element of Array : " + newArr[0]);
	}

	public static void findArrayByIndex(int[] arr, int index) {
		int i;
		int newArr[] = new int[1];
		for (i = 0; i < arr.length; i++) {
			if (i == index) {
				newArr[0] = arr[i];
			}
		}
		System.out.println("Find by Index of Array : " + newArr[0]);

	}

	public static void updateArrayByIndex(int[] arr, int index, int element) {
		int i;
		System.out.println("Update by Index of Array : ");
		int newArr[] = new int[arr.length];
		for (i = 0; i < arr.length; i++) {
			if (i == index) {
				newArr[i] = element;
			} else {
				newArr[i] = arr[i];
			}
		}
		for (int c = 0; c < newArr.length; c++) {
			System.out.println(" " + newArr[c]);
		}
	}

	public static void updateArrayByElement(int[] arr, int oldElement, int newElement) {
		int i;
		System.out.println("Update by Element of Array : ");
		int newArr[] = new int[arr.length];
		for (i = 0; i < arr.length; i++) {
			if (arr[i] == oldElement) {
				newArr[i] = newElement;
			} else {
				newArr[i] = arr[i];
			}
		}
		for (int j = 0; j < newArr.length; j++) {
			System.out.println(" " + newArr[j]);
		}
	}
}
