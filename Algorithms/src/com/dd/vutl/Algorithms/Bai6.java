package com.dd.vutl.Algorithms;

public class Bai6 {
	static int arrayInt[] = { 23, 17, 14, 29, 2, 31, 22 };

	public static void main(String[] args) {

		insertionSort(arrayInt);

	}

	public static void insertionSort(int[] arr) {

		for (int j = 1; j < arr.length; j++) {
			int index = arr[j];
			int i = j - 1;
			while ((i > -1) && (arr[i] > index)) {
				arr[i + 1] = arr[i];
				i--;
			}
			arr[i + 1] = index;
		}
		for (int i : arrayInt) {
			System.out.print(i + " ");
		}
	}
}
