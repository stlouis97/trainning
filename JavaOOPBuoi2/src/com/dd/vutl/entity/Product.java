package com.dd.vutl.entity;

import java.util.ArrayList;
import java.util.List;



public class Product {
	public int id;
	public String name;
	public int categoryID;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCategoryID() {
		return categoryID;
	}

	public void setCategoryID(int categoryID) {
		this.categoryID = categoryID;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", categoryID=" + categoryID + "]";
	}

	public Product(int id, String name, int categoryID) {
		this.id = id;
		this.name = name;
		this.categoryID = categoryID;
		
	}
	public Product() {
		
	}
	public static List<Product> getProducts(){
		List<Product> result = new ArrayList<Product>();
		try {
			result.add(new Product(1, "Audi", 1));
			result.add(new Product(2, "BMW", 1));
			result.add(new Product(3, "Toyota", 2));
			result.add(new Product(4, "Lexus", 2));
			result.add(new Product(5, "Lamborghini", 3));
			result.add(new Product(6, "Ferrari", 3));
			result.add(new Product(7, "Ford", 5));
			result.add(new Product(8, "Hyundai", 6));
			result.add(new Product(9, "Nissan", 6));
			result.add(new Product(10, "Kia", 6));
			return result;
		} catch(Exception e) {
			System.out.println(e);
		}
		return result;
	}
}
