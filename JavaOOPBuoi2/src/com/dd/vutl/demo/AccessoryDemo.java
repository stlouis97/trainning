package com.dd.vutl.demo;

import java.util.ArrayList;

import com.dd.vutl.dao.Database;
import com.dd.vutl.entity.Accessory;

public class AccessoryDemo {
	static Database database = new Database();

	public static void main(String[] args) {
		printAccessory();
	}

	public static void createAccessory() {
		ArrayList<Accessory> accessory = new ArrayList<Accessory>();
		try {
			accessory.add(new Accessory(1, "Accessory1"));
			accessory.add(new Accessory(2, "Accessory2"));
			accessory.add(new Accessory(3, "Accessory3"));
			accessory.add(new Accessory(4, "Accessory4"));
			accessory.add(new Accessory(5, "Accessory5"));
			accessory.add(new Accessory(6, "Accessory6"));
			accessory.add(new Accessory(7, "Accessory7"));
			accessory.add(new Accessory(8, "Accessory8"));
			accessory.add(new Accessory(9, "Accessory9"));
			accessory.add(new Accessory(10, "Accessory10"));
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
		database.insertTable("accessory", accessory);

	}

	public static void printAccessory() {
		System.out.println(new Accessory(1, "Accessory1"));

	}
}
