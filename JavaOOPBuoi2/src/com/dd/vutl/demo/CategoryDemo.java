package com.dd.vutl.demo;

import java.util.ArrayList;

import com.dd.vutl.dao.Database;
import com.dd.vutl.entity.Category;

public class CategoryDemo {
	static Database database = new Database();
	public static void main(String[] args) {
		printCategory();
	}
	
	public static void createCategoryTest(){
		ArrayList<Category> category = new ArrayList<Category>();
		try {
			category.add(new Category(1, "Category1"));
			category.add(new Category(2, "Category2"));
			category.add(new Category(3, "Category3"));
			category.add(new Category(4, "Category4"));
			category.add(new Category(5, "Category5"));
			category.add(new Category(6, "Category6"));
			category.add(new Category(7, "Category7"));
			category.add(new Category(8, "Category8"));
			category.add(new Category(9, "Category9"));
			category.add(new Category(10, "Category10"));
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
		database.insertTable("category",category);
	}
	public static void printCategory() {
		System.out.println(new Category(1, "Category1"));
		
	}
}
