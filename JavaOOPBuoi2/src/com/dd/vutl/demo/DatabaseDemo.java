package com.dd.vutl.demo;

import java.util.ArrayList;

import com.dd.vutl.dao.Database;
import com.dd.vutl.entity.Accessory;
import com.dd.vutl.entity.Category;
import com.dd.vutl.entity.Product;

public class DatabaseDemo {
	static Database database = new Database();

	public static void main(String[] args) {
		insertTableTest();
		System.out.println("=====");
		selectTableTest();
		System.out.println("=====");
		deleteTableTest();
		System.out.println("=====");
		truncateTableTest();
		System.out.println("=====");
		initDatabaseTest();
		System.out.println("=====");
		printTableTest();
		System.out.println("=====");
		updateTableTest();
	}

	private static void insertTableTest() {
		Product product = new Product(1, "product1", 1);
		Product product2 = new Product(2, "product2", 2);
		database.insertTable("product", product);
		database.insertTable("product", product2);
		Category category = new Category(1, "category1");
		Category category2 = new Category(2, "category2");
		database.insertTable("category", category);
		database.insertTable("category", category2);
		Accessory accessory = new Accessory(1, "Accessory1");
		Accessory accessory1 = new Accessory(2, "Accessory2");
		database.insertTable("accessory", accessory);
		database.insertTable("accessory", accessory1);
	}

	public static void selectTableTest() {
		ArrayList<Object> products = database.selectTable("product");
		System.out.println(products);
		ArrayList<Object> category = database.selectTable("category");
		System.out.println(category);	
		ArrayList<Object> accessory = database.selectTable("accessory");
		System.out.println(accessory);
	}

	public static void updateTableTest() {
		ArrayList<Object> product = database.selectTable("product");
		Object oldProduct = product.get(1);
		Object newProduct = new Category(1,"Product20");
		database.updateTable("product", oldProduct,  newProduct);
		System.out.println("Old Product : "+oldProduct+"-->"+"New Product : "+newProduct);

	}

	public static void deleteTableTest() {
		ArrayList<Object> products = database.selectTable("product");
		products.remove(0);
		System.out.println(products);
	}

	public static void truncateTableTest() {
		database.truncateTable("category");
	}

	public static void initDatabaseTest() {
		ArrayList<Product> product = new ArrayList<Product>();
		try {
			product.add(new Product(1, "Audi", 1));
			product.add(new Product(2, "BMW", 1));
			product.add(new Product(3, "Toyota", 2));
			product.add(new Product(4, "Lexus", 2));
			product.add(new Product(5, "Lamborghini", 3));
			product.add(new Product(6, "Ferrari", 3));
			product.add(new Product(7, "Ford", 5));
			product.add(new Product(8, "Hyundai", 6));
			product.add(new Product(9, "Nissan", 6));
			product.add(new Product(10, "Kia", 6));
		} catch (Exception e) {
			System.out.println(e);
		}
		ArrayList<Category> category = new ArrayList<Category>();
		try {
			category.add(new Category(1, "Category1"));
			category.add(new Category(2, "Category2"));
			category.add(new Category(3, "Category3"));
			category.add(new Category(4, "Category4"));
			category.add(new Category(5, "Category5"));
			category.add(new Category(6, "Category6"));
			category.add(new Category(7, "Category7"));
			category.add(new Category(8, "Category8"));
			category.add(new Category(9, "Category9"));
			category.add(new Category(10, "Category10"));
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
		// Tao list accessory
		ArrayList<Accessory> accessory = new ArrayList<Accessory>();
		try {
			accessory.add(new Accessory(1, "Accessory1"));
			accessory.add(new Accessory(2, "Accessory2"));
			accessory.add(new Accessory(3, "Accessory3"));
			accessory.add(new Accessory(4, "Accessory4"));
			accessory.add(new Accessory(5, "Accessory5"));
			accessory.add(new Accessory(6, "Accessory6"));
			accessory.add(new Accessory(7, "Accessory7"));
			accessory.add(new Accessory(8, "Accessory8"));
			accessory.add(new Accessory(9, "Accessory9"));
			accessory.add(new Accessory(10, "Accessory10"));
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
			database.insertTable("product", product);
			database.insertTable("category", category);
			database.insertTable("accessory", accessory);

	}

	public static void printTableTest() {
		System.out.println("Print Table : ");
		ArrayList<Object> product = database.selectTable("product");
		ArrayList<Object> category = database.selectTable("category");
		ArrayList<Object> accessory = database.selectTable("accessory");
		

		System.out.println(product);
		System.out.println(category);
		System.out.println(accessory);

	}
}
