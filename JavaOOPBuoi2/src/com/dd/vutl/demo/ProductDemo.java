package com.dd.vutl.demo;

import java.util.ArrayList;

import com.dd.vutl.dao.Database;
import com.dd.vutl.entity.Product;

public class ProductDemo {
	static Database database = new Database();
	public static void main(String[] args) {
		createProductTest();
		printProduct();
		
	}
	
	public static void createProductTest() {
		ArrayList<Product> product = new ArrayList<Product>();
		try {
			product.add(new Product(1, "Audi", 1));
			product.add(new Product(2, "BMW", 1));
			product.add(new Product(3, "Toyota", 2));
			product.add(new Product(4, "Lexus", 2));
			product.add(new Product(5, "Lamborghini", 3));
			product.add(new Product(6, "Ferrari", 3));
			product.add(new Product(7, "Ford", 5));
			product.add(new Product(8, "Hyundai", 6));
			product.add(new Product(9, "Nissan", 6));
			product.add(new Product(10, "Kia", 6));
		} catch (Exception e) {
			System.out.println(e);
		}
		database.insertTable("product", product);
	}
	public static void printProduct() {
		System.out.println(new Product(1, "Audi", 1));
		
	}
}
