package com.dd.vutl.dao;

import java.util.ArrayList;

public class Database {
	public ArrayList<Object> productTable;
	public ArrayList<Object> categoryTable;
	public ArrayList<Object> accessoryTable;
	public Database instants;

	public Database() {
		this.productTable = new ArrayList<Object>();
		this.categoryTable = new ArrayList<Object>();
		this.accessoryTable = new ArrayList<Object>();
	}

	

	public Integer insertTable(String name, Object row) {
		int i = 1;
		if (name.equals("product")) {
			this.productTable.add(row);

		}
		if (name.equals("category")) {
			this.categoryTable.add(row);

		}
		if (name.equals("accessory")) {
			this.accessoryTable.add(row);

		}

		return i;
	}

	public ArrayList<Object> selectTable(String name) {
		if (name.equals("product")) {
			return this.productTable;
		}
		if (name.equals("category")) {
			return this.categoryTable;
		}
		if (name.equals("accessory")) {
			return this.accessoryTable;
		}
		return null;
	}

	public Integer updateTable(String name, Object oldRow, Object newRow) {
		int i = 0;

		ArrayList<Object> product = selectTable("product");

		Integer oldProduct = product.indexOf(oldRow);
		if (name.equals("product")) {
			this.productTable.set(oldProduct, newRow);
			i = 1;
		}
		if (name.equals("category")) {
			this.categoryTable.set(oldProduct, newRow);
			i = 1;
		}
		if (name.equals("accessory")) {
			this.accessoryTable.set(oldProduct, newRow);
			i = 1;
		}
		return i;

	}

	public boolean deleteTable(String name, Object row) {
		boolean check = true;
		if (name.equals("product")) {
			this.productTable.remove(row);
		}
		if (name.equals("category")) {
			this.categoryTable.remove(row);
		}
		if (name.equals("accessory")) {
			this.accessoryTable.remove(row);
		}

		return check;
	}

	public void truncateTable(String name) {
		if (name.equals("product")) {
			this.productTable.clear();
			System.out.println("Truncate product");
		}
		if (name.equals("category")) {
			this.categoryTable.clear();
			System.out.println("Truncate category");
		}
		if (name.equals("accessory")) {
			this.accessoryTable.clear();
			System.out.println("Truncate accessory");
		}
	}

}
